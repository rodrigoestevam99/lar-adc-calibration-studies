.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LAr ADC Calibration Studies
===========================

This Wiki is a documentation from the framework available at: https://gitlab.cern.ch/restevam/lar-ltdb-adc-calib

As described at `Jira issue 1096 <https://its.cern.ch/jira/browse/ATLLARONL-1096>`_ was found out that some of the LTDB's ADC channels had problems with linearity errors (DNL specificly).

A general investigation on all LTDB was necessary to verify the occorence of those errors in other channels and to evaluate the necessity of recalibration, as they can be reduced by loading the appropriate calibration constants.



The proposed framework is responsable to process LARDIGITS ntuples and extract linearity and noise information from LTDB (and Main Readout) ADC channels.


.. warning::
   This is a work in progress 




.. toctree::
   :maxdepth: 2
   :caption: Documentation
   :numbered:

   Introduction </Documentation/index>



.. toctree::
   :maxdepth: 2
   :caption: Analysis

   Metrics              </Analysis/metrics>
   Past Presentations   </Analysis/presentations>
   Results              </Analysis/results>



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
